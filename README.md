# PremiumExchanger Parser

1. Добавить страки в файл /premiumbox/moduls/parser_settings/birg_filters.php:

$links['rapira'] = array(
    'title' => 'Rapira',
    'url' => 'https://api.rapira.net/open/market/rates',
    'birg_key' => 'rapira',
);

2. Изменить строку в конце файла:

`if(strstr($birg_key,'xmlc_'))`
на
`if(strstr($birg_key,'xmlc_') || strstr($birg_key,'rapira'))`